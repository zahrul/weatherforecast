import React from 'react';
import { 
  StyleSheet, 
  Alert,
} from 'react-native';
import { API_KEY } from './src/utils/WeatherAPIKey';
import Geolocation from '@react-native-community/geolocation';
import Weather from './src/components/Weather';
import AppNavigator from './src/components/AppNavigator'

export default class App extends React.Component {
  state = {
    isLoading: false,
    jsonData: null,
    city: '',
    temperature: '0',
    weatherCondition: null,
    initialPosition: 'unknown',
    error: null
  };

  componentDidMount() {
    Geolocation.getCurrentPosition(
      position => {
        const initialPosition = JSON.stringify(position);
        this.setState({initialPosition});
        console.log(initialPosition);
        this.fetchWeather(position.coords.latitude, position.coords.longitude);
      },
      // error => {
      //   this.setState({
      //     error: 'Error Getting Weather Condtions'
      //   });
      // }
      error => Alert.alert('Error', JSON.stringify(error)),
      {enableHighAccuracy: false, timeout: 20000, maximumAge: 1000},
    );
  }

  fetchWeather = async (lat, lon) => {
    var location = 'London';
    await fetch(
      //`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${API_KEY}&units=metric`
      `http://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&appid=${API_KEY}&units=metric`
    )
      .then(res => res.json())
      .then(json => {
        //console.log(json);
        this.setState({
          jsonData: json,
          city: json.city.name,
          temperature: parseInt(json.list[0].main.temp),
          weatherCondition: json.list[0].weather[0].main,
          isLoading: true
        });
      });
  }

  render() {
    return (
      <AppNavigator/>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  }
});