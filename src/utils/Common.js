const Common = {
  getWeatherIcon: (condition) => {
    var weatherIcon = {
      'rain': 'weather-pouring',
      'clouds': 'weather-cloudy',
      'clear': 'weather-sunny'
    }
    return weatherIcon[condition.toLowerCase()]
  },

  getDayAndDate: (datetimeString) => {
    var unixTimestamp = Date.parse(datetimeString.replace(/-/g, '/')) / 1000;
    var datetime = new Date(parseInt(unixTimestamp) * 1000);
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var date = datetime.getDate();
    var month = months[datetime.getMonth()];
    var year = datetime.getFullYear();
    var day = days[datetime.getDay()];
    var formattedDatestring = ' ' + date + ' ' + month + ' ' + year;

    return [day, formattedDatestring]
  },

  getTimeAMPM: (hour24format) => {
    var times = {
      '00': '12 AM',
      '03': '3 AM',
      '06': '6 AM',
      '09': '9 AM',
      '12': '12 PM',
      '15': ' 3 PM',
      '18': ' 6 PM',
      '21': ' 9 PM',
    }
    return times[hour24format]
  },

  getBackgroundColor: (condition) => {
    var color = {
      'rain': '#292929',
      'clouds': '#ABABA4',
      'clear': '#F3B84A'
    }
    return color[condition.toLowerCase()]
  }
}

export default Common;