import React from 'react';
import { 
  View, 
  Text, 
  StyleSheet, 
  TouchableOpacity,
  ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Common from '../utils/Common'

export default class Weather extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    var ctr = 1;
    return (
      <View style={styles.weatherContainer}>
        <TouchableOpacity 
          style={styles.headerContainer}
          onPress={() => this.props.navigate('WeatherDetails', {
              index: 0,
              weatherData: this.props.jsonData
            })
          }
        >
          <Text style={styles.name}>{this.props.jsonData.city.name}</Text>
          <Text style={styles.name}>{Common.getDayAndDate(this.props.jsonData.list[0].dt_txt)}</Text>
          <Text style={styles.tempText}>{parseInt(this.props.jsonData.list[0].main.temp)}˚</Text>
          <Text style={styles.weatherCondition}>{this.props.jsonData.list[0].weather[0].main}</Text>
          <Icon
            name={Common.getWeatherIcon(this.props.jsonData.list[0].weather[0].main)}
            color="white"
            size={50}
          />
        </TouchableOpacity>
        <View style={styles.bodyContainer}>
          {
            this.props.jsonData.list.map((list, index) => {
              let firstItemIndex = [2, 10, 18, 24]
              if (list.dt_txt.slice(11, 13) == '00' && ctr <= 4) {
                ctr += 1;
                let dayAndDate = Common.getDayAndDate(list.dt_txt);
                return(
                  <TouchableOpacity
                    key={index}
                    style={styles.itemContainer}
                    onPress={() => this.props.navigate('WeatherDetails', {
                        index: index,
                        weatherData: this.props.jsonData
                      })
                    }
                  >
                    <View style={{flexDirection: 'column'}}>
                      <Text style={styles.itemDayAndDate}>
                        {dayAndDate}
                      </Text>
                      <Text style={styles.itemTemp}>
                        {parseInt(list.main.temp_min)}˚C - {parseInt(list.main.temp_max)}˚C
                      </Text>
                      <Text style={styles.itemWeather}>
                        {list.weather[0].main}
                      </Text>
                    </View>
                    <Icon
                      name={Common.getWeatherIcon(list.weather[0].main)}
                      color="white"
                      size={50}
                    />
                  </TouchableOpacity>
                )
              }
              else {
                return null
              }
            })
          }
        </View>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  weatherContainer: {
    flex: 1,
    backgroundColor: '#32769B'
  },
  headerContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  name: {
    fontSize: 18,
    color: '#fff',
  },
  tempText: {
    fontSize: 48,
    color: '#fff'
  },
  weatherCondition: {
    fontSize: 16,
    color: '#fff'
  },
  bodyContainer: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemContainer: {
    marginTop: 10,
    justifyContent: 'center',
    borderTopWidth: 1,
    height: 90,
    width: 320,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderColor: '#fff',
    borderWidth: 0.2,
  },
  itemDayAndDate: {
    color: '#fff',
    fontSize: 18
  },
  itemTemp: {
    color: '#fff',
    fontSize: 15
  },
  itemWeather: {
    color: '#fff',
    fontSize: 13
  }
});