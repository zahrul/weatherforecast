import React from 'react';
import { 
    View, 
    Text, 
    StyleSheet,
    Animated
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Common from '../utils/Common'

export default class WeatherDetails extends React.Component {
  constructor(props) {
    super(props)

    this.Animation = new Animated.Value(0)
  }

  componentDidMount()  {
      this.StartBackgroundColorAnimation();
  }

  StartBackgroundColorAnimation = () =>
  {
      //this.Animation.setValue(0);
      Animated.timing(
        this.Animation,
        {
          toValue: 1,
          duration: 5000,
          useNativeDriver: false
        }
      ).start(() => { this.StartBackgroundColorAnimation() });
  }

  render() {
    var ctr = 1;
    const {params} = this.props.navigation.state
    var startIndex = params.index
    const backgroundColorConfig = this.Animation.interpolate(
    {
      inputRange: [ 0, 1 ],
      outputRange: ['#32769B', Common.getBackgroundColor(params.weatherData.list[params.index].weather[0].main)]
    });
    return (
      <Animated.View style={[styles.weatherContainer, {backgroundColor: backgroundColorConfig}]} >
        <View style={styles.headerContainer}>
          <Text style={styles.name}>{params.weatherData.city.name}</Text>
          <Text style={styles.name}>{Common.getDayAndDate(params.weatherData.list[params.index].dt_txt)}</Text>
          <Text style={styles.tempText}>{parseInt(params.weatherData.list[params.index].main.temp)}˚</Text>
          <Text style={styles.weatherCondition}>{params.weatherData.list[params.index].weather[0].main}</Text>
          <Icon
            name={Common.getWeatherIcon(params.weatherData.list[params.index].weather[0].main)}
            color="white"
            size={50}
          />
        </View>
        <View style={styles.bodyContainer}>
          {
            params.weatherData.list.map((list, index) => {
              // To display first index of the day and the next 24 hours forecast
              if (index == startIndex && ctr <= 8) {
                startIndex += 1;
                ctr += 1;
                return (
                  <View 
                    key={index}
                    style={styles.itemContainer}
                  >
                    <View style={styles.itemRow}>
                      <Text style={[styles.rowText, {width: 80}]}>
                        {Common.getTimeAMPM(list.dt_txt.slice(11, 13))}
                      </Text>
                      <Icon
                        name={Common.getWeatherIcon(list.weather[0].main)}
                        color="white"
                        size={25}
                      />
                      <Text style={styles.rowText}>
                        {parseInt(list.main.temp_min) + '    ' + parseInt(list.main.temp_max)}
                      </Text>
                    </View>
                  </View>
                )
              }
              else {
                return null
              }
            })
          }
        </View>
      </Animated.View>
    );
  }
};

const styles = StyleSheet.create({
  weatherContainer: {
    flex: 1,
    //backgroundColor: '#32769B'
  },
  headerContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderColor: '#fff'
  },
  name: {
    fontSize: 18,
    color: '#fff',
  },
  tempText: {
    fontSize: 48,
    color: '#fff'
  },
  weatherCondition: {
    fontSize: 16,
    color: '#fff'
  },
  bodyContainer: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemContainer: {
    marginTop: 5,
    justifyContent: 'center',
    height: 40,
    width: 280,
    borderColor: '#fff',
  },
  itemRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  rowText: {
    color: '#fff',
    fontSize: 17,
  },
});