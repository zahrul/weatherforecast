import React, {Component} from 'react'
import {
  TouchableOpacity
} from 'react-native'
import Weather from './Weather'
import Home from './Home'
import WeatherDetails from './WeatherDetails'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import Icon from 'react-native-vector-icons/Ionicons'

const AppStackNavigator = createStackNavigator(
  {
    Home: {
      screen: Home,
      headerMode: 'none',
      navigationOptions: {
        header: null
      }
    },
    WeatherDetails: {
      screen: WeatherDetails,
      navigationOptions: {
        headerTransparent: true,
        // headerLeft: ({goBack}) => (
        //   <TouchableOpacity
        //     style={{marginLeft: 10}}
        //     onPress={() => {goBack()}}
        //   >
        //     <Icon
        //         name='arrow-back-circle-sharp'
        //         color="white"
        //         size={40}
        //     />
        //   </TouchableOpacity>
        // )
      }
    },
  },
//   {
//     headerMode: 'none',
//     navigationOptions: {
//       headerVisible: false,
//     }
//   },
  {
    defaultNavigationOptions: {
      headerTintColor: 'white',
    //   headerStyle: {
    //     backgroundColor: 'grey',
    //   },
    },
    initialRouteName: 'Home'
  }
)

const AppNavigator = createAppContainer(AppStackNavigator)
export default AppNavigator