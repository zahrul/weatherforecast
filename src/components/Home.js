import React from 'react';
import { 
  StyleSheet, 
  Text, 
  View, 
  Alert,
  ActivityIndicator
} from 'react-native';
import { API_KEY } from '../utils/WeatherAPIKey';
import Geolocation from '@react-native-community/geolocation';
import Weather from './Weather';

export default class Home extends React.Component {
  state = {
    isLoading: false,
    jsonData: null,
    error: null
  };

  componentDidMount() {
    Geolocation.getCurrentPosition(
      position => {
        const initialPosition = JSON.stringify(position);
        this.setState({initialPosition});
        console.log(initialPosition);
        this.fetchWeather(position.coords.latitude, position.coords.longitude);
      },
      // error => {
      //   this.setState({
      //     error: 'Error Getting Weather Condtions'
      //   });
      // }
      error => Alert.alert('Error', JSON.stringify(error)),
      {enableHighAccuracy: false, timeout: 20000, maximumAge: 1000},
    );
  }

  fetchWeather = async (lat, lon) => {
    var location = 'London';
    await fetch(
      `http://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&appid=${API_KEY}&units=metric`
    )
      .then(res => res.json())
      .then(json => {
        //console.log(json);
        this.setState({
          jsonData: json,
          isLoading: true
        });
      });
  }

  render() {
    const { navigate } = this.props.navigation;
    let props = {
      navigate: navigate,
      jsonData: this.state.jsonData,
    }
    return (
      <View style={styles.container}>
        { !this.state.isLoading ? 
          <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', flexDirection: 'column'}}>
            <ActivityIndicator 
              color='#32769B'
              size= 'large'
            />
            <Text style={{fontSize: 15}}>Fetching Weather Data</Text>
          </View> :
          <Weather {...props} />
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  }
});